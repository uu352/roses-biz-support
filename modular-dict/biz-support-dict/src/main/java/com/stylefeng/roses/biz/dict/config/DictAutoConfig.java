package com.stylefeng.roses.biz.dict.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 字典的自动配置
 *
 * @author fengshuonan
 * @date 2018-07-24-下午5:12
 */
@Configuration
@ComponentScan("com.stylefeng.roses.biz.dict")
public class DictAutoConfig {

}
